
package com.linkpower.healthcross.demoservice.jaxws;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.linkpower.healthcross.demoservice.jaxws package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _GetHbsDataResponse_QNAME = new QName("http://service.healthcross.linkpower.com/", "getHbsDataResponse");
    private final static QName _ListHbsDateResponse_QNAME = new QName("http://service.healthcross.linkpower.com/", "listHbsDateResponse");
    private final static QName _GetHbsData_QNAME = new QName("http://service.healthcross.linkpower.com/", "getHbsData");
    private final static QName _ListHbsDate_QNAME = new QName("http://service.healthcross.linkpower.com/", "listHbsDate");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.linkpower.healthcross.demoservice.jaxws
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link ListHbsDate }
     * 
     */
    public ListHbsDate createListHbsDate() {
        return new ListHbsDate();
    }

    /**
     * Create an instance of {@link ListHbsDateResponse }
     * 
     */
    public ListHbsDateResponse createListHbsDateResponse() {
        return new ListHbsDateResponse();
    }

    /**
     * Create an instance of {@link GetHbsData }
     * 
     */
    public GetHbsData createGetHbsData() {
        return new GetHbsData();
    }

    /**
     * Create an instance of {@link GetHbsDataResponse }
     * 
     */
    public GetHbsDataResponse createGetHbsDataResponse() {
        return new GetHbsDataResponse();
    }

    /**
     * Create an instance of {@link HbsDemoData }
     * 
     */
    public HbsDemoData createHbsDemoData() {
        return new HbsDemoData();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetHbsDataResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service.healthcross.linkpower.com/", name = "getHbsDataResponse")
    public JAXBElement<GetHbsDataResponse> createGetHbsDataResponse(GetHbsDataResponse value) {
        return new JAXBElement<GetHbsDataResponse>(_GetHbsDataResponse_QNAME, GetHbsDataResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ListHbsDateResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service.healthcross.linkpower.com/", name = "listHbsDateResponse")
    public JAXBElement<ListHbsDateResponse> createListHbsDateResponse(ListHbsDateResponse value) {
        return new JAXBElement<ListHbsDateResponse>(_ListHbsDateResponse_QNAME, ListHbsDateResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetHbsData }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service.healthcross.linkpower.com/", name = "getHbsData")
    public JAXBElement<GetHbsData> createGetHbsData(GetHbsData value) {
        return new JAXBElement<GetHbsData>(_GetHbsData_QNAME, GetHbsData.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ListHbsDate }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service.healthcross.linkpower.com/", name = "listHbsDate")
    public JAXBElement<ListHbsDate> createListHbsDate(ListHbsDate value) {
        return new JAXBElement<ListHbsDate>(_ListHbsDate_QNAME, ListHbsDate.class, null, value);
    }

}
