package com.linkpower.healthcross.service;

import java.util.Date;
import java.util.List;

import javax.jws.WebService;

import com.linkpower.healthcross.model.HbsDemoData;

/***
 * Implementation of DemoService
 * @author Phoenix
 *
 */
@WebService(endpointInterface = "com.linkpower.healthcross.service.DemoService",
serviceName = "DemoService")
public class DemoServiceImpl implements DemoService
{
	/*
	 * (non-Javadoc)
	 * @see com.linkpower.healthcross.service.DemoService#getHbsData(long)
	 */
	public HbsDemoData getHbsData(long recordId)
	{
		System.out.println("Service getHbsData starting...");
		
		HbsDemoData data = new HbsDemoData();
		
		data.setId(recordId);
		data.setName("Tom Lee");
		data.setAge(18);
		data.setBirthday(new Date(System.currentTimeMillis() - 18 * 365 * 24 * 3600 * 1000l));
		data.setGender('M');
		data.setRemark("Tom lee is a good boy!");
		
		System.out.println("Service getHbsData finished successfully!");
		
		return data;
	}

	/*
	 * (non-Javadoc)
	 * @see com.linkpower.healthcross.service.DemoService#listHbsDate(long, java.util.Date)
	 */
	public List<HbsDemoData> listHbsDate(long customerId, Date date)
	{
		// TODO Auto-generated method stub
		return null;
	}

}
