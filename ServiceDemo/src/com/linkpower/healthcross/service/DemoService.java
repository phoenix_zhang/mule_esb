package com.linkpower.healthcross.service;

import java.util.Date;
import java.util.List;

import javax.jws.WebService;

import com.linkpower.healthcross.model.HbsDemoData;

/***
 * Interface of DemoService
 * @author Phoenix
 *
 */
@WebService
public interface DemoService
{
	/***
	 * Get a record
	 * @param recordId
	 * @return
	 */
	HbsDemoData getHbsData(long recordId);
	
	/***
	 * Get a list of records with given customer and date
	 * @param customerId
	 * @param date
	 * @return
	 */
	List<HbsDemoData> listHbsDate(long customerId, Date date);
}
