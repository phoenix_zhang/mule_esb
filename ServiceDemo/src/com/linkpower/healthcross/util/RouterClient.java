package com.linkpower.healthcross.util;

import java.io.IOException;

import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import com.linkpower.healthcross.model.HbsDemoData;

public class RouterClient
{
	private final static String ROUTER_SERVICE_URL = "http://localhost:8089/?";

	public HbsDemoData getDataFromWebservice(int serverId, long recordId)
	{
		HbsDemoData result = null;

		HttpClient httpclient = new DefaultHttpClient();		
		
		StringBuffer url = new StringBuffer(ROUTER_SERVICE_URL);
		url.append("serverid=").append(serverId).append("&");
		url.append("recordid=").append(recordId);
		
		HttpGet httpGet = new HttpGet(url.toString());
		try {
			HttpResponse response = httpclient.execute(httpGet);
			
			Header[] headers = response.getAllHeaders();
			for(Header h : headers){
				System.out.println("Header[" + h.getName() + "]=" + h.getValue());
			}
			
			HttpEntity entity = response.getEntity();
			System.out.println("Response content type is " + entity.getContentType());
			
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return result;
	}
	
	public static void main(String[] args)
	{
		RouterClient rc = new RouterClient();
		int serverId = 2;
		long recordId = 8;
		rc.getDataFromWebservice(serverId, recordId);
	}

}
