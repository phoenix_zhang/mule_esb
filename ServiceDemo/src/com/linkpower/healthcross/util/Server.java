package com.linkpower.healthcross.util;

import javax.xml.ws.Endpoint;

import com.linkpower.healthcross.service.DemoServiceImpl;

public class Server
{
    protected Server() throws Exception {
        // START SNIPPET: publish
        System.out.println("Starting Server");
        DemoServiceImpl implementor = new DemoServiceImpl();
        String address = "http://www.hbsservice.com:9000/hbsservice";
        Endpoint.publish(address, implementor);
        // END SNIPPET: publish
    }
    
	public static void main(String[] args) throws Exception
	{
        Server server = new Server();
        System.out.println("Server ready...");
        
        Thread.sleep(5 * 3600 * 1000);
        System.out.println("Server exiting");
        System.exit(0);
	}

}
