package com.linkpower.healthcross.util;

import javax.xml.namespace.QName;
import javax.xml.ws.Service;
import javax.xml.ws.soap.SOAPBinding;

import com.linkpower.healthcross.model.HbsDemoData;
import com.linkpower.healthcross.service.DemoService;

public class ProxyClient
{
	private static final QName SERVICE_NAME = new QName("http://service.healthcross.linkpower.com/", "DemoService");
	private static final QName PORT_NAME = new QName("http://service.healthcross.linkpower.com/", "DemoServicePort");

	private ProxyClient()
	{
	}

	public static void main(String args[]) throws Exception
	{
		
		Service service = Service.create(SERVICE_NAME);
		// Endpoint Address
		String endpointAddress = "http://localhost:8888/hbsservice";
		System.out.println("This will test the webservice("
				+ endpointAddress + 
				") directly without MuleESB...");
		
		
		// If web service deployed on Tomcat (either standalone or embedded)
		// as described in sample README, endpoint should be changed to:
		// String endpointAddress =
		// "http://localhost:8080/java_first_jaxws/services/hello_world";

		// Add a port to the Service
		service.addPort(PORT_NAME, SOAPBinding.SOAP11HTTP_BINDING, endpointAddress);

		long recordId = 1l;
		DemoService hw = service.getPort(DemoService.class);
		HbsDemoData data = hw.getHbsData(recordId);
		System.out.println(data.toString());

	}
}
