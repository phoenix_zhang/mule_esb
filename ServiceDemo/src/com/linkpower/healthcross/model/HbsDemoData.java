package com.linkpower.healthcross.model;

import java.io.Serializable;
import java.util.Date;

/***
 * Data model for HBS in IAL HK
 * @author Phoenix
 *
 */
public class HbsDemoData implements Serializable
{
	private static final long serialVersionUID = -2049873928498395239L;

	private Long id;
	private String name;
	private Integer age;
	private Character gender;
	private Date birthday;
	private String remark;
	
	
	public Long getId()
	{
		return id;
	}
	public void setId(Long id)
	{
		this.id = id;
	}
	public String getName()
	{
		return name;
	}
	public void setName(String name)
	{
		this.name = name;
	}
	public Integer getAge()
	{
		return age;
	}
	public void setAge(Integer age)
	{
		this.age = age;
	}
	public Character getGender()
	{
		return gender;
	}
	public void setGender(Character gender)
	{
		this.gender = gender;
	}
	public Date getBirthday()
	{
		return birthday;
	}
	public void setBirthday(Date birthday)
	{
		this.birthday = birthday;
	}
	public String getRemark()
	{
		return remark;
	}
	public void setRemark(String remark)
	{
		this.remark = remark;
	}
	
	public String toString()
	{
		StringBuffer result = new StringBuffer("");
		
		result.append("id=" + id).append(", name=" + name)
			.append(", age=" + age)
			.append(", gender=" + gender)
			.append(", birthday=" + birthday)
			.append(", remark=" + remark);
		
		return result.toString();
	}
}
